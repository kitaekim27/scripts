#!/bin/bash

set -o errexit -o nounset -o noglob -o pipefail

THIS_SCRIPT=$(basename "${BASH_SOURCE[0]}")

source "${SCRIPTS_DIR}/lib/bash/defer.sh"
source "${SCRIPTS_DIR}/lib/bash/output.sh"

ASSETS_DIR="/usr/local/var/lib/${THIS_SCRIPT}"
CACHE_TARBALL="${ASSETS_DIR}/alpine.tar"
CUSTOM_TARBALL="${ASSETS_DIR}/custom.tar"
CONTENTS_DIR="${ASSETS_DIR}/contents"

ALPINE_MIRROR="https://dl-cdn.alpinelinux.org"
ARCH="x86_64"

LINUX_RELEASE=""
OUTPUT=""
WILL_REFRESH_CACHE="false"

function print_usage {
	cat <<-EOF
		Usage: ${THIS_SCRIPT} [options...] <output> <kernel-release>

		OPTIONS:
		    -r : Refresh Alpine Linux tarball cache.
	EOF
}

function parse_args {
	while getopts "r" opt; do
		case "${opt}" in
			r)
				WILL_REFRESH_CACHE="true"
				;;
			\?)
				panic "Invalid argument: ${OPTARG}"
				;;
			:)
				panic "Missing required parameter: ${OPTARG}"
				;;
		esac
	done

	shift $((OPTIND - 1))
}

function get_tarball {
	local script_parse="
import sys
import yaml
releases = yaml.safe_load(sys.stdin.read())
print(next(x for x in releases if x.get('flavor') == 'alpine-minirootfs').get('file'))
	"

	# TODO: Make sure file format is correct.
	local tarball_url
	tarball_url=$(
		curl --silent --fail-with-body \
			"${ALPINE_MIRROR}/alpine/latest-stable/releases/${ARCH}/latest-releases.yaml" \
			| python -c "${script_parse}"
	)

	curl --fail-with-body --output "${CACHE_TARBALL}" \
		"${ALPINE_MIRROR}/alpine/latest-stable/releases/${ARCH}/${tarball_url}"
}

function customize_tarball {
	local tmp_dir
	tmp_dir=$(mktemp --directory --tmpdir "${THIS_SCRIPT}.XXXXXXXXXX")
	defer "rm --recursive \"${tmp_dir}\""

	tar --extract --directory="${tmp_dir}" --file="${CACHE_TARBALL}" \
		--preserve-permissions --xattrs-include="*.*" --numeric-owner

	echo "@testing ${ALPINE_MIRROR}/alpine/edge/testing" >>"${tmp_dir}/etc/apk/repositories"
	cp --dereference /etc/resolv.conf "${tmp_dir}/etc/resolv.conf"

	ichroot "${tmp_dir}" apk update
	ichroot "${tmp_dir}" apk add cryptsetup
	ichroot "${tmp_dir}" apk add tpm2-tools@testing
	# tpm2-tools do dlopen() to libtss2-tcti-device.so~ based on the type of the tpm device (e.g.,
	# mssim, swtpm, ...). But, tpm2-tools package does not contain these tcti libraries. Instead,
	# tpm2-tss-tcti-device package does.
	ichroot "${tmp_dir}" apk add tpm2-tss-tcti-device

	mkdir --parents "$(dirname "${CUSTOM_TARBALL}")"
	tar --create --file="${CUSTOM_TARBALL}" --directory="${tmp_dir}" \
		--preserve-permissions --xattrs --numeric-owner .
}

function build_initramfs {
	local tmp_dir
	tmp_dir=$(mktemp --directory --tmpdir "${THIS_SCRIPT}.XXXXXXXXXX")
	defer "rm --recursive \"${tmp_dir}\""

	tar --extract --directory="${tmp_dir}" --file="${CUSTOM_TARBALL}" \
		--preserve-permissions --xattrs-include="*.*" --numeric-owner

	cp --verbose --recursive --no-target-directory "${CONTENTS_DIR}" "${tmp_dir}"

	mkdir --parents "${tmp_dir}/lib/modules/${LINUX_RELEASE}"
	cp --verbose --recursive --no-target-directory \
		"/lib/modules/${LINUX_RELEASE}" \
		"${tmp_dir}/lib/modules/${LINUX_RELEASE}"

	mkdir --parents "${tmp_dir}/lib/firmware"
	cp --verbose --recursive --no-target-directory /lib/firmware "${tmp_dir}/lib/firmware"

	local output_abspath
	output_abspath=$(realpath --canonicalize-missing "${OUTPUT}")
	mkdir --parents "$(dirname "${output_abspath}")"
	(cd "${tmp_dir}" && find . -mindepth 1 -print0 \
		| cpio --null --create --format="newc" \
		| gzip >"${output_abspath}")
}

function main {
	[[ "${EUID}" != 0 ]] && panic "This script requires root privilege!"

	parse_args
	OUTPUT="${1:-NULL}"
	LINUX_RELEASE="${2:-NULL}"

	if [[ "${OUTPUT}" == "NULL" ]] || [[ "${LINUX_RELEASE}" == "NULL" ]]; then
		print_usage
		exit 1
	fi

	[[ "${WILL_REFRESH_CACHE}" == "true" ]] || [[ ! -r "${CACHE_TARBALL}" ]] && get_tarball
	[[ "${WILL_REFRESH_CACHE}" == "true" ]] || [[ ! -r "${CUSTOM_TARBALL}" ]] && customize_tarball
	build_initramfs
	println "Done."
}

main "${@}"
