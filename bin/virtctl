#!/bin/bash

set -o errexit -o nounset -o noglob -o pipefail

THIS_SCRIPT=$(basename "${BASH_SOURCE[0]}")

source "${SCRIPTS_DIR}/lib/bash/output.sh"

XDG_DATA_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}"
VM_DATA_DIR="${XDG_DATA_HOME}/${THIS_SCRIPT}"

function print_usage {
	cat <<-EOF
		${THIS_SCRIPT} [ACTION] [ARGS...]

		ACTIONS:
		    create <vm_name> <disk_size>
		        Create a virtual machine with the given name and disk size.

		    list
		        List availabe virtual machines.

		    run <vm_name>
		        Run a virtual machine with the given name.
	EOF
}

function create_vm {
	local vm_name="${1}"
	local size="${2}"

	local vm_dir="${VM_DATA_DIR}/${vm_name}"
	local swtpm_dir="${vm_dir}/swtpm"

	# TODO: It seems Windows11 requires secureboot-capable firmware (e.g.
	# OVMF_CODE.secboot.4m.fd). But as we don't have any secureboot key, it does
	# not boot. Use OVMF_CODE for now.
	local ovmf_code="/usr/share/edk2/x64/OVMF_CODE.4m.fd"
	local ovmf_vars="/usr/share/edk2/x64/OVMF_VARS.4m.fd"

	[[ -r "${ovmf_code}" ]] || panic "Can not read OVMF code."
	[[ -r "${ovmf_vars}" ]] || panic "Can not read OVMF variable store."

	mkdir --parents "${vm_dir}" "${swtpm_dir}"
	ln --symbolic "${ovmf_code}" "${vm_dir}/OVMF_CODE.fd"
	cp "${ovmf_vars}" "${vm_dir}/OVMF_VARS.fd"
	chmod 0666 "${vm_dir}/OVMF_VARS.fd"
	qemu-img create -f qcow2 "${vm_dir}/disk.qcow2" "${size}"
}

function list_vm {
	find "${VM_DATA_DIR}" -mindepth 1 -maxdepth 1 -type d -not -name ".*" \
		-exec realpath --relative-to "${VM_DATA_DIR}" {} \;
}

function run_vm {
	local vm_name="${1}"
	local vm_dir="${VM_DATA_DIR}/${vm_name}"

	[[ -r "${vm_dir}/disk.qcow2" ]] || panic "Can not read storage image."
	[[ -r "${vm_dir}/OVMF_CODE.fd" ]] || panic "Can not read OVMF code."
	[[ -r "${vm_dir}/OVMF_VARS.fd" ]] || panic "Can not read OVMF variable store."

	local flags=()

	flags+=(-machine pc)
	flags+=(-cpu host)
	flags+=(-accel kvm)
	flags+=(-smp cores=$(($(nproc) / 2)))
	flags+=(-m $(($(awk '$1=="MemTotal:"{print $2}' /proc/meminfo) / 2))k)

	flags+=(-drive "if=pflash,format=raw,unit=0,file=${vm_dir}/OVMF_CODE.fd,readonly=on")
	flags+=(-drive "if=pflash,format=raw,unit=1,file=${vm_dir}/OVMF_VARS.fd")
	flags+=(-drive file="${vm_dir}/disk.qcow2")
	[[ -r "${vm_dir}/cd.iso" ]] && flags+=(-cdrom "${vm_dir}/cd.iso")

	flags+=(-device "virtio-vga-gl,xres=2880,yres=1620")
	flags+=(-display "sdl,gl=on")

	# This is required by qemu guest agent.
	# flags+=(-chardev socket,path="${vm_dir}/qga.sock",server=on,wait=off,id=qga0)
	# flags+=(-device virtio-serial)
	# flags+=(-device virtserialport,chardev=qga0,name=org.qemu.guest_agent.0)

	# For spice.
	# flags+=(-vga qxl)
	# flags+=(-display "spice-app,gl=on")
	# These are required for spice agent in the guest to work properly.
	# flags+=(-device virtio-serial-pci)
	# flags+=(-chardev "spicevmc,id=vdagent,debug=0,name=vdagent")
	# flags+=(-device "virtserialport,chardev=vdagent,name=com.redhat.spice.0")

	flags+=(-audio "pa,model=hda")

	local swtpm_dir="${vm_dir}/swtpm"
	local swtpm_sock="${swtpm_dir}/sock"

	swtpm socket \
		--tpm2 \
		--tpmstate dir="${swtpm_dir}" \
		--ctrl type=unixio,path="${swtpm_sock}" \
		--log level=20,file="${swtpm_dir}/log" &

	while [[ ! -r "${swtpm_sock}" ]]; do
		sleep 0.01
	done

	flags+=(-chardev "socket,id=chrtpm,path=${swtpm_sock}")
	flags+=(-tpmdev "emulator,id=tpm0,chardev=chrtpm")
	flags+=(-device "tpm-tis,tpmdev=tpm0")

	# OVMF emits a number of info / debug messages to the QEMU debug console, at
	# ioport 0x402. We configure qemu so that the debug console is indeed
	# available at that ioport. We redirect the host side of the debug console to
	# a file.
	flags+=(-global "isa-debugcon.iobase=0x402")
	flags+=(-debugcon "file:${vm_dir}/OVMF.log")

	qemu-system-x86_64 "${flags[@]}"
}

function main {
	mkdir --parents "${VM_DATA_DIR}"

	case "${1:-NULL}" in
		create)
			create_vm "${2}" "${3}"
			;;
		list)
			list_vm
			;;
		run)
			run_vm "${2}"
			;;
		*)
			print_usage
			;;
	esac
}

main "${@}"
