#!/bin/bash

_DEFERRED_SCRIPTS=("${_DEFERRED_SCRIPTS[@]}")

function defer {
	_DEFERRED_SCRIPTS=("${1}" "${_DEFERRED_SCRIPTS[@]}")
}

function _eval_deferred_scripts {
	while [[ "${#_DEFERRED_SCRIPTS[@]}" -gt 0 ]]; do
		local command="${_DEFERRED_SCRIPTS[0]}"
		_DEFERRED_SCRIPTS=("${_DEFERRED_SCRIPTS[@]:1:${#_DEFERRED_SCRIPTS[@]}}")
		eval "${command}"
	done
}

trap _eval_deferred_scripts EXIT
