#!/bin/bash

source "${SCRIPTS_DIR}/lib/bash/colors.sh"

function print {
	printf "${COLOR_GREEN}${THIS_SCRIPT:-unknown}${COLOR_CLEAR}: %s" "${@}"
}

function println {
	printf "${COLOR_GREEN}${THIS_SCRIPT:-unknown}${COLOR_CLEAR}: %s\n" "${@}"
}

function eprintln {
	printf "${COLOR_RED}${THIS_SCRIPT:-unknown}${COLOR_CLEAR}: %s\n" "${@}" >&2
}

function panic {
	eprintln "${@}"
	exit 1
}
